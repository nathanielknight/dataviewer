# Data Viewer

A Sinatra/Angular app for viewing structured directories of images with
relevant, structured names. If you have a folder full of images whose filenames
are underscore-separated fields with a consistent structure, you can put it
in the `data/` directory, fire up the server with `ruby app.rb` (requires Sinatra)
and view them easily in a browser.

Example images are provided so you can see how it works.

Some quirks to be aware of:

 * The client expects everything to be a PNG, but the browser is usually clever
   enough to figure out what kind of file is being served.

