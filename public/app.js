var viewerApp = angular.module("viewerApp", []);

//Fetches a schema from the server and presents it in options


viewerApp.controller("DatasetsCtrl", function($scope, $http){
    //Fetch schema from server
    $scope.data = $http.get("/schema").then(function(response){
        $scope.data = response.data;
        $scope.sets = Object.keys(response.data);
    });
    
    //Initialization
    $scope.current_set = ["none"];
    $scope.current_params = [];
    $scope.param_groups = $scope.data[$scope.current_set];
    $scope.filename = "";


    //Methods
    $scope.set_current_set = function(set){
        $scope.current_set = set;
        $scope.current_params = $scope.data[set].map(function(x){ return x[0]});
        $scope.param_groups = $scope.data[set]
    }
    
    $scope.set_param = function(param, param_group){
        i = $scope.param_groups.indexOf(param_group); 
        k = $scope.param_groups[i].indexOf(param)
        $scope.current_params[i] = $scope.data[$scope.current_set][i][k];
        $scope.set_filename();
    }
    
    
    $scope.set_filename = function(){
        fname = $scope.current_params.join("_") + ".png"
        dirname = "/data/" + $scope.current_set+ "/";
        $scope.filename = dirname + fname;
    }


});
