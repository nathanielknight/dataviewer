require 'sinatra'
require 'json'
require 'set'


def get_parts(dir)
  puts '================================================================================'
  puts dir
  filenames = Dir["#{dir}/*"].map {|x| x.split("/").last}
  parts = filenames.map do |f|
    f.split('.').first.split('_')
  end
  schema = []
  (0..(parts[0].length - 1)).each do |i|
    puts schema
    schema << Set[*parts.map {|p| p[i]} .select {|p| not p.nil?}].to_a.sort
  end
  return schema
end

def get_schema(datadir)
  schema = {}
  Dir["#{datadir}/*"].each do |dir|
    schema[dir.split('/').last] = get_parts(dir)
  end
  return schema
end


#Settings===============================================
data_dir = "./data"


#Responses==============================================
get '/' do
  send_file "./public/index.html"
end
get '/schema' do
  content_type :json
  get_schema(data_dir).to_json
end

get '/data/:dataset/:filename' do |dataset, filename|
  send_file "./data/#{dataset}/#{filename}"
end

set :public_folder, File.dirname(__FILE__) + "/public"
